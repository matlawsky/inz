import os
import sys
import glob
import serial
import time
import re
import logging
import subprocess
from decouple import config

PYTHON_LOCATION = config('PYTHON_LOCATION')

logging.basicConfig(filename="comms.logs",
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)

logging.info("Running communication server...")

logger = logging.getLogger('CommsServer')

# functions outside the connection but involving filesystem of a microcontroller
def serial_ports():
    """
    TESTED
    Listing serial port names
    """

    if sys.platform.startswith("win"):
        ports = ["COM%s" % (i + 1) for i in range(256)]
    elif sys.platform.startswith("linux") or sys.platform.startswith("cygwin"):
        # this excludes current terminal "/dev/tty"
        ports = glob.glob("/dev/tty[A-Za-z]*")
    elif sys.platform.startswith("darwin"):
        ports = glob.glob("/dev/tty.*")
    else:
        raise EnvironmentError("Unsupported platform")

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except Exception as e:
            logging.info(f"Try connect device: {e}")
            pass
    logging.info(f"Connected devices: {result}")
    return result

# using ampy
def run_file(port:str, file_path:str):
    """
    Run file on a device
    """
    try:
        os.system(f"ampy --port {port} run --no-output {file_path}")
    except Exception as exception:
        print(exception)
        return False

    return 1

def put_file(port:str, file_path:str):
    """
    Put file or folder on a device, can handle multiple files
    """
    try:
        os.system(f"ampy --port {port} put {file_path}")

    except Exception as exception:
        print(exception)
        return False

def copy_file_from_device(port:str, file_path_1:str, file_path_2:str):
    """
    Copy file from a device to device
    """
    try:
        # command = f"ampy --port {port} put --no-output " + " ".join(file_paths)
        command = f"ampy --port {port} get {file_path_1} {file_path_2}"
        os.system(command)

    except Exception as exception:
        print(exception)
        return False
    
    return 1

def make_directory_on_device(port, directory:str):
    """
    Copy file from a device to device
    """
    try:
        # command = f"ampy --port {port} put --no-output " + " ".join(file_paths)
        command = f"ampy --port {port} mkdir {directory}"
        os.system(command)

    except Exception as exception:
        print(exception)
        return False
    
    return 1

def list_files_on_device(port:str, location=""):
    """
    List files on device
    """
    try:
        # commands = ["ampy", "--port", port, "ls"] 
        commands = f"ampy --port {port} ls"
        if location != "":
            commands = f"{commands} {location}"
            # commands = commands + [location]
        out = os.popen(commands).read()
            
        # proc = subprocess.Popen(commands, stdout=subprocess.PIPE, shell=True)
        # (out, err) = proc.communicate()
        # print("program output:", out)
        print(out)
        # print(err)
        return out.split()
    except Exception as exception:
        print(exception)
        return False

def remove_files_on_device(port:str, location:str):
    """
    Remove files or directories from device
    """
    try:
        commands = f"ampy --port {port} rm {location}"
        # commands = [PYTHON_LOCATION, "ampy", "--port", port, "rm"]
        os.system(commands)

    except Exception as exception:
        print(exception)
        return False
    
    return 1

def remove_dir_on_device(port:str, location:str):
    """
    Remove files or directories from device
    """
    try:
        commands = f"ampy --port {port} rm {location}"
        # commands = [PYTHON_LOCATION, "ampy", "--port", port, "rmdir"]
        os.system(commands)

    except Exception as exception:
        print(exception)
        return False
    
    return 1


class Connection:
    """
    Serial or Wifi connection initiator
    """
    def __init__(self, port="COM3", baudrate=115200, timeout=60, devicetype="Micropython", con_type="serial"):
        self.p = port
        self.baud = baudrate
        self.tout = timeout
        self.devicetype = devicetype
        self.con_type = con_type
        self.session = serial.Serial

    def connect(self):
        """
        Base connection method,
        for now only serial implemented
        """
        try:
            if self.con_type in "serial":
                # Establish a serial connection
                ser = serial.Serial(port=self.p, baudrate=self.baud, timeout=self.tout)
                # Wait for the connection to establish
                time.sleep(2)
                command = b'help()\r'
                ser.write(command)
                logging.info(f"Command: {command}")
                # Wait for the command to execute and response to come back
                time.sleep(5)
                self.session = ser
                print(f"Type of connection is: {type(ser)}")
                res = ser.read_all().decode()
                logging.info(f"Command: {res}")
            elif self.con_type == 'wireless':
                # self.session = ConnectHandler(**self.device)
                return 'Not implemented'
            else:
                return 'Wrong connection idea :)'
        except ConnectionRefusedError as e:
            logging.info(f"Error: {e}")
            return 'ConnectionRefusedError'
        except ValueError as e:
            logging.info(f"Error: {e}")
            return 'ValueError'
        except TimeoutError as e:
            logging.info(f"Error: {e}")
            return 'TimeoutError'
        except OSError as e:
            logging.info(f"Error: {e}")
            return 'OSError'

        return self

    def disconnect(self):
        """
        Base dissconnection method,
        for now only serial implemented,
        used when needed is use of ampy
        """
        try:
            # Close a serial connection
            self.session.close()
            time.sleep(2)
            logging.info(f"Disconnected from device on port: {self.p}")

        except Exception as e:
            logging.info(f"Error: {e}")
            return 'DisconnectError'

        return self

    def comm(self, message):
        """
        send single line to repl
        """
        try:
            ser = self.session
            message = f"{message}\r"
            b = message.encode("utf-8")
            time.sleep(3)
            ser.write(b)
            return ser.read_until(b'\r').decode()
        except Exception as e:
            return f"Error: {e}"

    def comm_file(self, file_path):
        """
        Reads a Python file and returns a list of lines that are send line by line to repl
        """
        try:
            ser = self.session
            commands = []
            with open(file_path, 'r') as file:
                for line_number, line in enumerate(file, start=1):
                    if line.strip() == '':  # Checking if the line is empty or contains only whitespaces
                        commands.append(f"{line}\r")

            for c in commands:
                b = c.encode("utf-8")
                time.sleep(3)
                ser.write(b)

            return ser.read_until(b'\r').decode()
        except Exception as e:
            return f"Error: {e}"   

    def reset(self):
        try:
            ser = self.session
            commands = [
                f"import machine\r",
                f"machine.reset()\r",
            ]
            for c in commands:
                b = c.encode("utf-8")
                time.sleep(3)
                ser.write(b)
            return ser.read_until(b'\r').decode()
        except Exception as e:
            return f"Error: {e}"

    def webrepl(self):
        pass

    # dont use it for implementation
    def start_and_set_pin_value(self, name="var", pin=12, value=0, direction="Pin.OUT"):
        """
        name of the variable
        """
        try:
            ser = self.session
            commands = [
                "from machine import Pin\r",
                f"{name} = Pin({pin}, {direction}, value={value})\r",
                f"{name}.value({value})\r"
            ]
            for c in commands:
                b = c.encode("utf-8")
                time.sleep(3)
                ser.write(b)

            return ser.read_until(b'\r').decode()
        except Exception as e:
            return f"Error: {e}"

    def read_pin(self, name, read_type):
        try:
            ser = self.session
            commands = [
                f"print({name}.{read_type}())\r",
            ]
            for c in commands:
                b = c.encode("utf-8")
                time.sleep(3)
                ser.write(b)
            return ser.read_until(b'\r').decode()
        except Exception as e:
            return f"Error: {e}"

    def start_and_set_pin_input(self, name, direction, pin, pull):
        """
        name of the variable
        """
        try:
            ser = self.session
            commands = [
                f"{name} = Pin({pin}, {direction}, {pull})\r"
                f"print({name}.value())\r"
            ]
            for c in commands:
                b = c.encode("utf-8")
                time.sleep(3)
                ser.write(b)

            return ser.read_until(b'\r').decode()
        except Exception as e:
            return f"Error: {e}"

    def start_adc(self, name, pin):
        """
        name of the variable
        """
        try:
            ser = self.session
            commands = [
                "from machine import Pin, ADC\r",
                f"{name} = ADC(Pin({pin}))\r",
            ]
            for c in commands:
                b = c.encode("utf-8")
                time.sleep(3)
                ser.write(b)

            return ser.read_until(b'\r').decode()
        except Exception as e:
            return f"Error: {e}"

    def read_adc(self, name, read_type):
        try:
            ser = self.session
            commands = [
                f"print({name}.{read_type}())\r",
            ]
            for c in commands:
                b = c.encode("utf-8")
                time.sleep(3)
                ser.write(b)
            return ser.read_until(b'\r').decode()
        except Exception as e:
            return f"Error: {e}"

    def setup_pwm(self, name, pin, duty_cycle, frequency):
        try:
            ser = self.session
            commands = [
                "from machine import Pin, PWM\r",
                f"{name} = PWM(Pin({pin}))\r",
                f"{name}.freq({frequency})\r",
                f"{name}.duty_u16({duty_cycle})\r"
                f"{name}.deinit()\r"
            ]
            for c in commands:
                b = c.encode("utf-8")
                time.sleep(3)
                ser.write(b)
            time.sleep(2)
            return ser.read_until(b'\r').decode()
        except Exception as e:
            return f"Error: {e}"
        
    def setup_irq_handler(self, name, led):
        """
        name of the variable
        """
        try:
            ser = self.session
            commands = [
                f"def irq_handler(pin):\r",
                f"    {led}.value({led}.value() ^ 1):\r",
                f"{name}.irq(irq_handler, Pin.IRQ_FALLING)\r",
            ]
            for c in commands:
                b = c.encode("utf-8")
                time.sleep(3)
                ser.write(b)

            return ser.read_until(b'\r').decode()
        except Exception as e:
            return f"Error: {e}"

    def start_timer(self, name, period, mode="Timer.PERIODIC", callback="lambda t:print('hello')"):
        """
        start timer
        """
        try:
            ser = self.session
            commands = [
                "from machine import Timer\r",
                f"{name} = Timer(period={period},mode={mode},callback={callback})\r"
            ]
            for c in commands:
                b = c.encode("utf-8")
                time.sleep(3)
                ser.write(b)

            return ser.read_until(b'\r').decode()
        except Exception as e:
            return f"Error: {e}"

    # def adc():
    #     pass
    # def led_state(self, port, led, state, baudrate=9600, timeout=1):
    #     try:
    #         with serial.Serial(port, baudrate, timeout=timeout) as ser:
    #             command = f'set_led({led}, {state})\r'.encode()
    #             ser.write(command)
    #             time.sleep(2)
    #             return ser.read_all().decode()
    #     except Exception as e:
    #         return f"Error: {e}"
    # Oh if you have a main loop that never exits (the while True) ampy will get confused waiting for the program to finish and eventually timeout. 
    # In those cases add the -n option which tells amp not to wait for output. For example:
    # ampy run -n foo.py

# if __name__ == "__main__":
#     result = serial_ports()
#     print(result)

#     for r in result:
#         c = Connection(r, timeout=60)
#         #  C:\Users\matla\inz\scripts\device_files\I2C\I2C1-OLED\I2C1-OLED.py
#         c.run_file('C:\\Users\\matla\\inz\\scripts\\device_files\\I2C\\I2C1-OLED\\I2C1-OLED.py')

        # c.connect()
        # c.reset()
        # c.disconnect()
        # time.sleep(15)

        # c.run_file('C:\\Users\\matla\\inz\\scripts\\device_files\\GPIO\\D2-BUZZER\\D2-BUZZER.py')

        # c.connect()
        # c.reset()
        # c.disconnect()
        # time.sleep(15)

        # c.run_file('C:\\Users\\matla\\inz\\scripts\\device_files\\GPIO\\D1-LED\\D1-LED.py')

        c.connect()
        c.reset()
        c.disconnect()
        # print(a)
