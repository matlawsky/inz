import json
from umqtt.simple import MQTTClient
from machine import SPI, UART, Pin, I2C

# Configuration for MQTT
MQTT_BROKER = "HOST_ADDRESS"
MQTT_TOPIC = "config/topic"
CLIENT_ID = "ESP32_client"

# Initialize SPI, UART, I2C to None (will be configured on demand)
spi = None
uart = None
i2c = None

def setup_spi(config):
    global spi
    sck = Pin(config['sck'], Pin.OUT)
    mosi = Pin(config['mosi'], Pin.OUT)
    miso = Pin(config['miso'], Pin.IN)
    spi = SPI(baudrate=config['baudrate'], polarity=config['polarity'], phase=config['phase'], sck=sck, mosi=mosi, miso=miso)
    print("SPI configured with baudrate {}, polarity {}, and phase {}".format(config['baudrate'], config['polarity'], config['phase']))

def setup_uart(config):
    global uart
    uart = UART(config['uart_id'], baudrate=config['baudrate'])
    uart.init(baudrate=config['baudrate'], bits=config['bits'], parity=config['parity'], stop=config['stop'])
    print("UART configured with baudrate {}".format(config['baudrate']))

def setup_i2c(config):
    global i2c
    scl = Pin(config['scl'], Pin.OUT)
    sda = Pin(config['sda'], Pin.IN)
    i2c = I2C(scl=scl, sda=sda, freq=config['freq'])
    print("I2C configured with frequency {}".format(config['freq']))

def on_message(topic, msg):
    print("Received message '{}' on topic '{}'".format(msg, topic))
    try:
        data = json.loads(msg)
        if 'spi' in data:
            setup_spi(data['spi'])
        if 'uart' in data:
            setup_uart(data['uart'])
        if 'i2c' in data:
            setup_i2c(data['i2c'])
    except Exception as e:
        print("Failed to process message: {}".format(str(e)))

def connect_and_subscribe():
    client = MQTTClient(CLIENT_ID, MQTT_BROKER)
    client.set_callback(on_message)
    client.connect()
    client.subscribe(MQTT_TOPIC)
    print("Connected to {} and subscribed to {}".format(MQTT_BROKER, MQTT_TOPIC))
    return client

def main():
    client = connect_and_subscribe()
    try:
        while True:
            client.wait_msg() # change to check for msg
    finally:
        client.disconnect()

if __name__ == "__main__":
    main()
