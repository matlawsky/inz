import ujson
import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO

# Define the MQTT topics for command and response
MQTT_COMMAND_TOPIC = "gpio/command"
MQTT_RESPONSE_TOPIC = "gpio/response"

# MQTT Broker details
MQTT_BROKER = "HOST_ADDRESS"
MQTT_PORT = 1883

# Setup GPIO in a safe mode
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

# Dictionary to hold GPIO pin states and configuration
gpio_pins = {}

def setup_gpio(pin, mode):
    """Setup a GPIO pin with the specified mode."""
    GPIO.setup(pin, GPIO.OUT if mode == "out" else GPIO.IN)
    gpio_pins[pin] = {"mode": mode, "state": GPIO.input(pin)}

def update_gpio(pin, state):
    """Update the GPIO pin state."""
    GPIO.output(pin, state)
    gpio_pins[pin]["state"] = state

def read_gpio(pin):
    """Read and return the GPIO pin state."""
    state = GPIO.input(pin)
    gpio_pins[pin]["state"] = state
    return state

def on_connect(client, userdata, flags, rc):
    """The callback for when the client connects to the broker."""
    print("Connected with result code " + str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(MQTT_COMMAND_TOPIC)

def on_message(client, userdata, msg):
    """The callback for when a PUBLISH message is received from the server."""
    print(f"Message received `{msg.payload}` from topic `{msg.topic}`")
    try:
        command = ujson.loads(msg.payload)
        response = {}
        
        for gpio, action in command.items():
            pin = int(gpio.replace("gpio", ""))
            
            if action == "out":
                setup_gpio(pin, GPIO.OUT)
                state = read_gpio(pin)
                response[gpio] = "HIGH" if state else "LOW"
            elif action == "in":
                setup_gpio(pin, GPIO.IN)
                state = read_gpio(pin)
                response[gpio] = "HIGH" if state else "LOW"
            elif action in ["0", "1"]:
                update_gpio(pin, int(action))
                response[gpio] = "Set to HIGH" if int(action) else "Set to LOW"
            else:
                response[gpio] = "Invalid command"
        
        response_json = ujson.dumps(response)
        client.publish(MQTT_RESPONSE_TOPIC, response_json)
    except Exception as e:
        print(f"Failed to process message: {str(e)}")

def on_disconnect(client, userdata, rc):
    """The callback for when the client disconnects from the broker."""
    print(f"Disconnected with result code {rc}")

def main():
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    
    client.connect(MQTT_BROKER, MQTT_PORT, 60)
    client.loop_forever()

if __name__ == "__main__":
    main()
