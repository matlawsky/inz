from machine import Pin, I2C
from umqtt.simple import MQTTClient
import time

# SHTC3 sensor commands
SHTC3_REG_SLEEP = 0xB098
SHTC3_REG_WAKEUP = 0x3517
SHTC3_REG_SOFTRESET = 0x805D
SHTC3_REG_READID = 0xEFC8
SHTC3_REG_NORMAL_T_F = 0x7866
SHTC3_REG_NORMAL_H_F = 0x58E0
SHTC3_REG_NORMAL_T_F_STRETCH = 0x7CA2
SHTC3_REG_NORMAL_H_F_STRETCH = 0x5C24
SHTC3_REG_LOWPOWER_T_F = 0x609C
SHTC3_REG_LOWPOWER_H_F = 0x401A
SHTC3_REG_LOWPOWER_T_F_STRETCH = 0x6458
SHTC3_REG_LOWPOWER_H_F_STRETCH = 0x44DE

SHTC3_MEAS_ALL = [
    [[SHTC3_REG_NORMAL_T_F, SHTC3_REG_NORMAL_H_F], [SHTC3_REG_LOWPOWER_T_F, SHTC3_REG_LOWPOWER_H_F]],
    [[SHTC3_REG_NORMAL_T_F_STRETCH, SHTC3_REG_NORMAL_H_F_STRETCH], [SHTC3_REG_LOWPOWER_T_F_STRETCH, SHTC3_REG_LOWPOWER_H_F_STRETCH]]
]

class SHTC3:
    def __init__(self, i2c_num=0, i2c_scl=9, i2c_sda=8, address=0x70):
        self.i2c = I2C(id=i2c_num, scl=Pin(i2c_scl, pull=Pin.PULL_UP), sda=Pin(i2c_sda, pull=Pin.PULL_UP), freq=100000)
        self._address = address
        self.cmd = bytearray(2)
        self.buffer = bytearray(6)

    @staticmethod
    def crc8(buffer):
        crc = 0xFF
        for byte in buffer:
            crc ^= byte
            for _ in range(8):
                if crc & 0x80:
                    crc = (crc << 1) ^ 0x31
                else:
                    crc <<= 1
        return crc & 0xFF

    def write_command(self, command):
        self.cmd[0] = command >> 8
        self.cmd[1] = command & 0xFF
        self.i2c.writeto(self._address, self.cmd)

    def measurement(self, hum_first=False, low_power_meas=False, stretch=False):
        command = SHTC3_MEAS_ALL[stretch][low_power_meas][hum_first]
        self.write_command(command)
        time.sleep_ms(2 if low_power_meas else 14)
        self.buffer = self.i2c.readfrom(self._address, 6)
        temp_data = self.buffer[hum_first*3:hum_first*3+2]
        hum_data = self.buffer[(not hum_first)*3:(not hum_first)*3+2]

        if self.crc8(temp_data) != self.buffer[hum_first*3+2] or self.crc8(hum_data) != self.buffer[(not hum_first)*3+2]:
            return (0, 0)  # Handle CRC error
        T_RAW = int.from_bytes(temp_data, 'big')
        RH_RAW = int.from_bytes(hum_data, 'big')
        T = (T_RAW * 175.0) / 65536 - 45
        RH = (RH_RAW * 100.0) / 65536
        return (T, RH)

def main():
    sensor = SHTC3()
    client = MQTTClient("rpi", "HOST_ADDRESS")
    client.connect()
    topic = "sensor/data"

    while True:
        T, RH = sensor.measurement()
        if T != 0 and RH != 0:
            client.publish(topic, f"Temperature: {T:.2f} C, Humidity: {RH:.2f} %")
        time.sleep(0.5)

if __name__ == '__main__':
    main()
