import threading
import time
import random
from paho.mqtt import client as mqtt_client

# Configuration for MQTT Broker
broker = '127.20.0.1'
port = 1883
topic = "python/mqtt"
client_id_prefix = "python_mqtt_"
username = 'mateo'
password = 'mateo'

# Function to connect MQTT client to the broker
def connect_mqtt(client_id):
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print(f"Client {client_id} connected to MQTT Broker!")
        else:
            print(f"Failed to connect, return code {rc}\n")

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

# Function for publishing messages
def publish(client, payload="Hello MQTT"):
    def on_publish(client, userdata, result):
        print(f"Message published by {client._client_id}")

    client.on_publish = on_publish
    client.loop_start()
    client.publish(topic, payload)
    time.sleep(1)  # Wait for message to be published
    client.loop_stop()

# Function for subscribing and consuming messages
def subscribe(client):
    def on_message(client, userdata, msg):
        message = msg.payload.decode()
        print(f"Received message '{message}' on topic '{msg.topic}' by {client._client_id}")

    start_time = time.time()
    while (time.time() - start_time) < 120:

        client.subscribe(topic)
        client.on_message = on_message
        client.loop_start()
        time.sleep(2)  # Wait for message to be published
        client.loop_stop()



# Function to simulate multiple clients publishing messages
def simulate_high_load(number_of_clients, message_count):
    def run_simulation(client_id):
        client = connect_mqtt(client_id)
        for _ in range(message_count):
            message = f"Message from {client_id}"
            publish(client, message)
        client.disconnect()

    clients = []
    for i in range(number_of_clients):
        client_id = f"{client_id_prefix}{i}"
        thread = threading.Thread(target=run_simulation, args=(client_id,))
        clients.append(thread)
        thread.start()

    for client in clients:
        client.join()

# Testing connectivity with wrong credentials
def test_invalid_credentials():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Security issue: Client connected with wrong credentials!")
        else:
            print("Security check passed: Connection with wrong credentials failed.")

    bad_client = mqtt_client.Client("bad_client")
    bad_client.username_pw_set("wrong", "credentials")
    bad_client.on_connect = on_connect
    bad_client.connect(broker, port)
    bad_client.loop_start()
    time.sleep(2)  # Wait to see if connection is successful
    bad_client.loop_stop()
    bad_client.disconnect()

# Uncomment below function calls to execute the testing
connect_mqtt("test_client")
publish(connect_mqtt("publisher"))
subscribe(connect_mqtt("subscriber"))
simulate_high_load(10, 5)
test_invalid_credentials()

