# accounts/models.py
from django.contrib.auth.models import AbstractUser
from django.db import models
from django_cryptography.fields import encrypt


class CustomUser(AbstractUser):
    """
    Custom user model extending Django's AbstractUser.
    The email field is made optional, and additional fields can be added as needed.
    """
    email = models.EmailField('email address', blank=True, null=True)
    # security questions
    security_question_1 = models.CharField(default="",max_length=255)
    security_answer_1 = encrypt(models.CharField(default="",max_length=255))


    def __str__(self):
        return str(self.username)


# from django.db import models
# from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
# from django.utils.translation import gettext_lazy as _


# class CustomUser(AbstractBaseUser, PermissionsMixin):
#     """
#     Custom model to possibly easily change model in the future
#     """

#     ROLE_CHOICES = (
#         ('author', 'Author'),
#         ('editor', 'Editor'),
#         ('admin', 'Admin'),
#     )

#     username = models.CharField(
#         verbose_name=_("Username"), 
#         max_length=100,
#         unique=True
#     )
#     )
#     email = models.EmailField(
#         verbose_name=_("Email Address"),
#         max_length=100,
#         unique=True
#     )

#     role          = models.CharField(choices=ROLE_CHOICES, default="author", max_length=20)
#     is_active     = models.BooleanField(default=True)
#     is_staff      = models.BooleanField(default=False)
#     # is_superuser  = models.BooleanField(default=False)
#     date_joined   = models.DateTimeField(auto_now_add=True)
#     last_modified = models.DateTimeField(auto_now=True)

#     USERNAME_FIELD = 'email'
#     REQUIRED_FIELDS = ['username',]

#     def __str__(self):
#         return str(self.username)

#     def has_perm(self, perm, obj=None):
#         return True

#     def has_module_perms(self, app_label):
#         return True

#     class Meta:
#         verbose_name = "Account"
#         verbose_name_plural = "Acounts"
