from django.db import models
from django.conf import settings



class Topic(models.Model):
    """
    Represents a topic for categorization, which can be associated with devices and communication logs.
    """
    name = models.CharField(max_length=255, unique=True)
    subscribed = models.BooleanField(default=True, blank=True)

    def __str__(self):
        return self.name


class Device(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100, blank=True)
    # in fact this all is in regards to broker
    address = models.CharField(max_length=50, blank=True)
    port = models.CharField(max_length=50, blank=True)
    device_username = models.CharField(max_length=200, blank=True)
    device_password = models.CharField(max_length=200, blank=True)
    topics = models.ManyToManyField(Topic, blank=True, related_name='topics')
    tls = models.BooleanField(default=False, blank=True)

    def __str__(self):
        return f"{self.name} (User: {self.user.username})"

class DataPoint(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    data = models.JSONField() 


class CodeFile(models.Model):
    # name = models.CharField(max_length=100)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1 ,on_delete=models.CASCADE)
    file = models.FileField(upload_to="code/")
    # tags = models.ManyToManyField(Tag,  blank=True, related_name='codes')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    # signals = models.ManyToManyField(Signal, blank=True, related_name='generated_by')
    def __str__(self):
        return f"{self.file.name} - {self.uploaded_at}"

