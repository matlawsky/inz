# serializers
from rest_framework import serializers
from koncepto.models import Device, CodeFile, DataPoint, Topic
from users.models import CustomUser


class CustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['id', 'username', 'email', 'password']
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        user = CustomUser.objects.create_user(
            username=validated_data['username'],
            email=validated_data.get('email'),
            password=validated_data['password']
        )
        return user


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)


class PasswordRecoverySerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    security_answer_1 = serializers.CharField(required=True)
    security_answer_2 = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def validate(self, data):
        # Retrieve user based on the username
        user = CustomUser.objects.filter(username=data['username']).first()
        if user:
            # The stored answers are automatically decrypted when accessed
            if user.security_answer_1 == data['security_answer_1']:
                # Correct answers
                return data
            else:
                # Incorrect answers
                raise serializers.ValidationError("Security answers are incorrect.")
        else:
            raise serializers.ValidationError("User not found.")


class RunFileSerializer(serializers.Serializer):
    port = serializers.CharField(required=True)
    file = serializers.FileField(required=False)

    def create(self, validated_data):
        # Handle file saving logic here
        instance = CodeFile.objects.create(file=validated_data['file_path'])
        return instance


class CopyToDeviceSerializer(serializers.Serializer):
    port = serializers.CharField(required=True)
    file_path = serializers.CharField(required=True)


class CopyFromDeviceSerializer(serializers.Serializer):
    port = serializers.CharField(required=True)
    file_path_1 = serializers.CharField(required=True)
    file_path_2 = serializers.CharField(required=True)
    print(f"Serializer: {port}")

    # def create(self, validated_data):
    #     user = CustomUser.objects.create_user(
    #         username=validated_data['username'],
    #         email=validated_data.get('email'),
    #         password=validated_data['password']
    #     )
    #     return user


class ListFilesSerializer(serializers.Serializer):
    port = serializers.CharField(required=True)
    location = serializers.CharField(required=False, allow_blank=True)


class MakeDirectorySerializer(serializers.Serializer):
    port = serializers.CharField(required=True)
    location = serializers.CharField(required=True)


class RemoveFilesSerializer(serializers.Serializer):
    port = serializers.CharField(required=True)
    location = serializers.CharField(required=True)


class RemoveDirectorySerializer(serializers.Serializer):
    port = serializers.CharField(required=True)
    location = serializers.CharField(required=True)


class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        fields = ['name', 'subscribed']


class DataPointSerializer(serializers.ModelSerializer):
    topic_name = serializers.CharField(write_only=True)  # Accept the name as input

    class Meta:
        model = DataPoint
        fields = ['topic_name', 'data', 'timestamp']

    def create(self, validated_data):
        topic_name = validated_data.pop('topic_name')  # Extract topic name from validated data
        topic, created = Topic.objects.get_or_create(name=topic_name)  # Create if not exists

        datapoint = DataPoint.objects.create(topic=topic, **validated_data)  # Create DataPoint with the topic
        return datapoint

class DeviceUpdateSerializer(serializers.ModelSerializer):
    topic_names = serializers.ListField(
        child=serializers.CharField(), 
        write_only=True,
        required=False  # Allow updating a device without changing topics
    )

    class Meta:
        model = Device
        fields = ['name', 'description', 'topic_names']

    def update(self, instance, validated_data):
        topic_names = validated_data.pop('topic_names', None)
        if topic_names is not None:
            # Find or create topics based on the provided names
            topics = [Topic.objects.get_or_create(name=name)[0] for name in topic_names]
            instance.topics.set(topics)  # Update the topics associated with this device

        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = '__all__'
        read_only_fields = ['user']  

# class DeviceSerializer(serializers.ModelSerializer):
#     topics = TopicSerializer(many=True, read_only=True)

#     class Meta:
#         model = Device
#         fields = ['id', 'name', 'user', 'description', 'address', 'port', 'topics', 'tls']
        # ["__all__"]
        # ['id', 'name', 'user', 'description', 'address', 'port', 'device_username', 'device_password', 'topics', 'tls']

# class DataPointSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = DataPoint
#         fields = ['id', 'topic', 'timestamp', 'data']

class CodeFileSerializer(serializers.ModelSerializer):
    # tags = serializers.PrimaryKeyRelatedField(many=True, queryset=Tag.objects.all())

    class Meta:
        model = CodeFile
        fields = ['user', 'file', 'uploaded_at']
        read_only_fields = ['user', 'uploaded_at']  # not to be editable via API
