import json
import os
from django.contrib.auth import authenticate
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import generics, permissions, status, viewsets
from rest_framework.throttling import AnonRateThrottle, UserRateThrottle
from users.models import CustomUser
from .serializers import CopyFromDeviceSerializer, CustomUserSerializer, DeviceSerializer, ListFilesSerializer, MakeDirectorySerializer, PasswordRecoverySerializer, RemoveDirectorySerializer, RemoveFilesSerializer, ChangePasswordSerializer, TopicSerializer
from functions_api.serial_communication import make_directory_on_device, serial_ports, run_file, put_file, copy_file_from_device, list_files_on_device, remove_dir_on_device, remove_files_on_device
from rest_framework.parsers import MultiPartParser, FormParser
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
import logging
from asgiref.sync import async_to_sync
from .serializers import (DeviceSerializer, 
                          DataPointSerializer, CodeFileSerializer)
from koncepto.models import Device, DataPoint, CodeFile, Topic

from django.http import FileResponse, HttpResponse, Http404
from django.conf import settings
from django.contrib.auth.decorators import login_required
import os


# USER VIEWS

class UserRegistrationView(APIView):
    def post(self, request):
        serializer = CustomUserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserLoginView(APIView):
    def post(self, request):
        # serializer = CustomUserSerializer(data=request.data)
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(username=username, password=password)
        if user:
            # Implement token generation logic here if using token authentication
            return Response({'message': 'Login Successful'}, status=status.HTTP_200_OK)
        return Response({'message': 'Invalid Credentials'}, status=status.HTTP_400_BAD_REQUEST)


class ChangePasswordView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        serializer = ChangePasswordSerializer(data=request.data)
        if serializer.is_valid():
            # Check old password
            if not request.user.check_password(serializer.validated_data['old_password']):
                return Response({'old_password': ['Wrong password.']}, status=status.HTTP_400_BAD_REQUEST)

            # Set new password
            request.user.set_password(serializer.validated_data['new_password'])
            request.user.save()
            return Response({'message': 'Password updated successfully'}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PasswordRecoveryView(APIView):
    throttle_classes = [AnonRateThrottle, UserRateThrottle]
    def post(self, request):
        serializer = PasswordRecoverySerializer(data=request.data)
        if serializer.is_valid():
            user = CustomUser.objects.get(username=serializer.validated_data['username'])
            user.set_password(serializer.validated_data['new_password'])
            user.save()
            return Response({'message': 'Password has been reset successfully.'}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# AMPY VIEWS

class SerialPortsView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def get(self, request):
        try:
            ports = serial_ports()
            return Response({'ports': ports}, status=status.HTTP_200_OK)
        except EnvironmentError as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class RunFileView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    # Set appropriate permissions
    parser_classes = (MultiPartParser, FormParser)
    # post is for file that is not uploaded but resides already in a known space
    # def post(self, request, *args, **kwargs):
    #     serializer = RunFileSerializer(data=request.data)
    #     if serializer.is_valid():
    #         try:
    #             run_file(serializer.validated_data['port'], serializer.validated_data['file_path'])
    #             return Response({'message': 'File run successfully'}, status=status.HTTP_200_OK)
    #         except Exception as e:
    #             logging.error(f'Exception occurred: {e}')
    #             return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    # put is for file that is uploaded and needs to be saved
    def put(self, request, *args, **kwargs):
        file_obj = request.data.get('file')
        port = request.data.get('port')
        # Save file to default storage
        file_name = default_storage.save(file_obj.name, ContentFile(file_obj.read()))
        file_path = default_storage.path(file_name)
        # serializer = RunFileSerializer(data=request.data)
        if not file_obj:
            return Response({"error": "No file provided"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            try:
                run_file(port, file_path)
                default_storage.delete(file_name)
                return Response({'message': 'File run successfully'}, status=status.HTTP_200_OK)
            except Exception as e:
                default_storage.delete(file_name)
                logging.error(f'Exception occurred: {e}')
                return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)


class CopyToDeviceView(APIView):
    # Set appropriate permissions
    permission_classes = [permissions.IsAuthenticated]
    parser_classes = (MultiPartParser, FormParser)
    # post is for file that is not uploaded but resides already in a known space
    def put(self, request, *args, **kwargs):
        file_obj = request.data.get('file')
        port = request.data.get('port')
        # Save file to default storage
        file_name = default_storage.save(file_obj.name, ContentFile(file_obj.read()))
        file_path = default_storage.path(file_name)
        
        # serializer = RunFileSerializer(data=request.data)
        if not file_obj:
            return Response({"error": "No file provided"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            try:
                put_file(port, file_path)
                default_storage.delete(file_name)
                return Response({'message': 'File transfered successfully'}, status=status.HTTP_200_OK)
            except Exception as e:
                logging.error(f'Exception occurred: {e}')
                default_storage.delete(file_name)
                return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)


class CopyFromDeviceView(APIView):
    # Set appropriate permissions
    permission_classes = [permissions.IsAuthenticated]
    def get(self, request):
        """
        {
            "file_path_1": "I2C1-OLED.py",
            "file_path_2": "C:\\Users\\matla\\inz\\backend\\functions_api\\OLED.py",
            "port":"COM7"
        }
        """
        serializer = CopyFromDeviceSerializer(data=request.data)
        if serializer.is_valid():
            try:
                copy_file_from_device(
                    port=str(serializer.validated_data['port']), 
                    file_path_1 = str(serializer.validated_data['file_path_1']), 
                    file_path_2 = str(serializer.validated_data['file_path_2']))
                return Response({'message': 'File run successfully'}, status=status.HTTP_200_OK)
            except Exception as e:
                logging.error(f'Exception occurred: {e}')
                return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListFilesView(APIView):
    # Set appropriate permissions
    permission_classes = [permissions.IsAuthenticated]
    def get(self, request):
        serializer = ListFilesSerializer(data=request.query_params)
        if serializer.is_valid():
            try:
                port = str(serializer.validated_data.get('port'))
                location = serializer.validated_data.get('location', "")  # Use a default value
                out = list_files_on_device(port=port, location=location)
                print(out)
                return Response({'filesystem': out}, status=status.HTTP_200_OK)
            except Exception as e:
                logging.error(f'Exception occurred: {e}')
                return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MakeDirectoryView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    # Set appropriate permissions
    def get(self, request):
        serializer = MakeDirectorySerializer(data=request.data)
        if serializer.is_valid():
            try:
                port = str(serializer.validated_data.get('port'))
                location = str(serializer.validated_data.get('location')) # Use a default value
                make_directory_on_device(port=port, directory=location)
                out = list_files_on_device(port)
                return Response({'Filesystem': out}, status=status.HTTP_200_OK)
            except Exception as e:
                logging.error(f'Exception occurred: {e}')
                return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RemoveFilesView(APIView):
    # Set appropriate permissions
    permission_classes = [permissions.IsAuthenticated]
    def get(self, request):
        serializer = RemoveFilesSerializer(data=request.data)
        if serializer.is_valid():
            try:
                remove_files_on_device(serializer.validated_data['port'], serializer.validated_data['location'])
                return Response({'message': 'File removed successfully'}, status=status.HTTP_200_OK)
            except Exception as e:
                logging.error(f'Exception occurred: {e}')
                return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RemoveDirectoryView(APIView):
    # Set appropriate permissions
    permission_classes = [permissions.IsAuthenticated]
    def get(self, request):
        serializer = RemoveDirectorySerializer(data=request.data)
        if serializer.is_valid():
            try:
                remove_dir_on_device(port=str(serializer.validated_data['port']), 
                                     location=str(serializer.validated_data['location']))
                return Response({'message': 'Directory removed successfully'}, status=status.HTTP_200_OK)
            except Exception as e:
                logging.error(f'Exception occurred: {e}')
                return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeviceViewSet(viewsets.ModelViewSet):
    serializer_class = DeviceSerializer
    permission_classes = [permissions.IsAuthenticated]
    # queryset =  Device.objects.all()
    def get_queryset(self):
        user = self.request.user
        return Device.objects.filter(user=user)
    
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            # Set the user to the current user
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

    def partial_update(self, request, pk=None):
        device = self.get_object()
        serializer = self.get_serializer(device, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # @action(detail=True, methods=['patch'], url_path='assign-topic')
    # def assign_topic(self, request, pk=None):
    #     device = self.get_object()
    #     topic_name = request.data.get('topic_name')
    #     topic, created = Topic.objects.get_or_create(name=topic_name)
    #     device.topics.add(topic)
    #     return Response({'status': 'topic added', 'topic_name': topic.name})


class DataPointViewSet(viewsets.ModelViewSet):
    queryset = DataPoint.objects.all()
    serializer_class = DataPointSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TopicViewSet(viewsets.ModelViewSet):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
    permission_classes = [permissions.IsAuthenticated]

class CodeFileViewSet(viewsets.ModelViewSet):
    serializer_class = CodeFileSerializer
    # queryset = CodeFile.objects.all()
    permission_classes = [permissions.IsAuthenticated]
    # parser_classes = (FileUploadParser,)  # Add FileUploadParser
    parser_classes = (MultiPartParser, FormParser)
    def get_queryset(self):
        return CodeFile.objects.filter(user=self.request.user)
    
    @action(methods=['post'], detail=False, url_path='upload')
    def upload_file(self, request, *args, **kwargs):
        file_obj = request.data.get('file')
        # Save file to default storage
        file_name = default_storage.save(file_obj.name, ContentFile(file_obj.read()))
        file_path = default_storage.path(file_name)
        # serializer = RunFileSerializer(data=request.data)
        if not file_obj:
            return Response({"error": "No file provided"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            try:
                serializer = self.get_serializer(data=request.data)
                if serializer.is_valid():
                    print("valid")
                    serializer.save()
                default_storage.delete(file_name)
                return Response({'message': 'File run successfully'}, status=status.HTTP_200_OK)
            except Exception as e:
                default_storage.delete(file_name)
                logging.error(f'Exception occurred: {e}')
                return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)


class TopicCreateUpdateView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def post(self, request, *args, **kwargs):
        serializer = TopicSerializer(data=request.data)
        if serializer.is_valid():
            topic_name = serializer.validated_data['name']
            topic, created = Topic.objects.get_or_create(name=topic_name)
            if not created:
                topic.subscribed = True
                topic.save()
            return Response(TopicSerializer(topic).data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TopicUnsubscribeView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    def post(self, request, *args, **kwargs):
        try:
            topic_name = request.data.get('name')
            topic = Topic.objects.get(name=topic_name)
            topic.subscribed = False
            topic.save()
            return Response(TopicSerializer(topic).data)
        except Topic.DoesNotExist:
            raise Http404


class ServeProtectedDocumentView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, file_name):
        file_path = os.path.join(settings.MEDIA_ROOT, 'code', file_name)
        if os.path.exists(file_path):
            try:
                with open(file_path, 'rb') as fh:
                    response = FileResponse(fh, content_type="application/python")
                    response['Content-Disposition'] = f'inline;filename={os.path.basename(file_path)}'
                    return response
            except IOError:
                raise Http404
        raise Http404


class FileContentView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, file_name):
        file_path = os.path.join(settings.MEDIA_ROOT, 'code', file_name)
        if not os.path.exists(file_path):
            raise Http404('File does not exist')

        try:
            return FileResponse(open(file_path, 'rb'), content_type='text/plain')
        except IOError:
            raise Http404