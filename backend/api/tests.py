from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from django.contrib.auth import get_user_model
from users.models import CustomUser
from koncepto.models import Device
from .serializers import CustomUserSerializer, ChangePasswordSerializer, PasswordRecoverySerializer, DeviceSerializer

class UserRegistrationViewTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.register_url = reverse('user-register')
        self.valid_payload = {
            'username': 'testuser',
            'email': 'testuser@example.com',
            'password': 'testpassword'
        }

    def test_user_registration_success(self):
        response = self.client.post(self.register_url, self.valid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_registration_invalid_data(self):
        invalid_payload = {
            'username': 'testuser',
            'password': 'testpassword'
        }
        response = self.client.post(self.register_url, invalid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # Add more test cases as needed

class UserLoginViewTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.login_url = reverse('user-login')
        self.user = CustomUser.objects.create_user(username='testuser', password='testpassword')

    def test_user_login_success(self):
        valid_payload = {
            'username': 'testuser',
            'password': 'testpassword'
        }
        response = self.client.post(self.login_url, valid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_login_invalid_credentials(self):
        invalid_payload = {
            'username': 'testuser',
            'password': 'wrongpassword'
        }
        response = self.client.post(self.login_url, invalid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # Add more test cases as needed

class ChangePasswordViewTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.change_password_url = reverse('change-password')
        self.user = CustomUser.objects.create_user(username='testuser', password='testpassword')
        self.client.force_authenticate(user=self.user)

    def test_change_password_success(self):
        valid_payload = {
            'old_password': 'testpassword',
            'new_password': 'newpassword'
        }
        response = self.client.post(self.change_password_url, valid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_change_password_invalid_old_password(self):
        invalid_payload = {
            'old_password': 'wrongpassword',
            'new_password': 'newpassword'
        }
        response = self.client.post(self.change_password_url, invalid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # Add more test cases as needed

class PasswordRecoveryViewTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.password_recovery_url = reverse('recover-password')
        self.user = CustomUser.objects.create_user(username='testuser', password='testpassword')
        self.valid_payload = {
            'username': 'testuser',
            'security_answer_1': 'answer1',
            'security_answer_2': 'answer2',
            'new_password': 'newpassword'
        }

    def test_password_recovery_success(self):
        response = self.client.post(self.password_recovery_url, self.valid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_password_recovery_invalid_answers(self):
        invalid_payload = {
            'username': 'testuser',
            'security_answer_1': 'wronganswer',
            'security_answer_2': 'wronganswer',
            'new_password': 'newpassword'
        }
        response = self.client.post(self.password_recovery_url, invalid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class DeviceListTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.device_list_url = reverse('device-list')
        self.user = get_user_model().objects.create_user(username='testuser', password='testpassword')
        self.client.force_authenticate(user=self.user)
        self.device = Device.objects.create(name='Test Device', user=self.user)

    def test_device_list_success(self):
        response = self.client.get(self.device_list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # Add more test cases as needed

class DeviceCreateTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.device_create_url = reverse('device-create')
        self.user = get_user_model().objects.create_user(username='testuser', password='testpassword')
        self.client.force_authenticate(user=self.user)
        self.valid_payload = {
            'name': 'New Device'
        }

    def test_device_create_success(self):
        response = self.client.post(self.device_create_url, self.valid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

class UserViewCreateTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user_view_create_url = reverse('user-view-create')
        self.user = get_user_model().objects.create_user(username='testuser', password='testpassword')
        self.client.force_authenticate(user=self.user)
        self.valid_payload = {
            'name': 'New User View'
        }

    def test_user_view_create_success(self):
        response = self.client.post(self.user_view_create_url, self.valid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    # Add more test cases as needed

class UserViewRetrieveUpdateDestroyTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(username='testuser', password='testpassword')
        self.user_view = UserView.objects.create(user=self.user, name='Test User View')
        self.user_view_retrieve_update_destroy_url = reverse('user-view-retrieve-update-destroy', args=[self.user_view.pk])
        self.client.force_authenticate(user=self.user)

    def test_user_view_retrieve_success(self):
        response = self.client.get(self.user_view_retrieve_update_destroy_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_view_update_success(self):
        updated_payload = {
            'name': 'Updated User View'
        }
        response = self.client.put(self.user_view_retrieve_update_destroy_url, updated_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(UserView.objects.get(pk=self.user_view.pk).name, updated_payload['name'])

    def test_user_view_delete_success(self):
        response = self.client.delete(self.user_view_retrieve_update_destroy_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(UserView.objects.filter(pk=self.user_view.pk).exists())


# class UserViewFileCreateTest(TestCase):
#     def setUp(self):
#         self.client = APIClient()
#         self.user_view_file_create_url = reverse('user-view-file-create')
#         self.user = get_user_model().objects.create_user(username='testuser', password='testpassword')
#         self.user_view = UserView.objects.create(user=self.user, name='Test User View')
#         self.client.force_authenticate(user=self.user)
#         self.valid_payload = {
#             'file': 'path/to/your/file.txt'
#         }

    def test_user_view_file_create_success(self):
        response = self.client.post(self.user_view_file_create_url, self.valid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    # Add more test cases as needed

# class UserViewFileRetrieveUpdateDestroyTest(TestCase):
#     def setUp(self):
#         self.client = APIClient()
#         self.user = get_user_model().objects.create_user(username='testuser', password='testpassword')
#         self.user_view = UserView.objects.create(user=self.user, name='Test User View')
#         self.user_view_file = UserViewFile.objects.create(user_view=self.user_view, file='path/to/your/file.txt')
#         self.user_view_file_retrieve_update_destroy_url = reverse('user-view-file-retrieve-update-destroy', args=[self.user_view_file.pk])
#         self.client.force_authenticate(user=self.user)

    # def test_user_view_file_retrieve_success(self):
    #     response = self.client.get(self.user_view_file_retrieve_update_destroy_url)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)

    # def test_user_view_file_update_success(self):
    #     updated_payload = {
    #         'file': 'path/to/updated/file.txt'
    #     }
    #     response = self.client.put(self.user_view_file_retrieve_update_destroy_url, updated_payload, format='json')
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(UserViewFile.objects.get(pk=self.user_view_file.pk).file, updated_payload['file'])

    # def test_user_view_file_delete_success(self):
    #     response = self.client.delete(self.user_view_file_retrieve_update_destroy_url)
    #     self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
    #     self.assertFalse(UserViewFile.objects.filter(pk=self.user_view_file.pk).exists())