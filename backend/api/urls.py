from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register(r'topics', views.TopicViewSet, basename='topic')
router.register(r'devices', views.DeviceViewSet, basename='device')
router.register(r'datapoints', views.DataPointViewSet, basename='dp')
router.register(r'code_files', views.CodeFileViewSet, basename='code_file')


urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', views.UserRegistrationView.as_view(), name='user-register'),
    path('login/', views.UserLoginView.as_view(), name='user-login'),
    path('change-password/', views.ChangePasswordView.as_view(), name='change-password'),
    path('recover-password/', views.PasswordRecoveryView.as_view(), name='recover-password'),
    # path('devices/<int:pk>/assign-topic/', views.DeviceViewSet.as_view({'patch': 'assign_topic'}), name='device-assign-topic'),
    # path('datapoints/', views.DataPointViewSet.as_view({'post': 'create'}), name='datapoint-create'),
    path('serial-ports/', views.SerialPortsView.as_view(), name='serial-ports'),
    path('run-file/', views.RunFileView.as_view(), name='run-file'),
    path('put-file/', views.CopyToDeviceView.as_view(), name='put-file'),
    path('copy-file/', views.CopyFromDeviceView.as_view(), name='copy-file'),
    path('list-files/', views.ListFilesView.as_view(), name='list-files'),
    path('make-directory/', views.MakeDirectoryView.as_view(), name='make-directory'),
    path('media/code/<str:file_name>', views.ServeProtectedDocumentView.as_view(), name='serve_protected_document'),
    path('file-content/<str:file_name>', views.FileContentView.as_view(), name='file_content'),
    path('topic/', views.TopicCreateUpdateView.as_view(), name='topic-create-update'),
    path('topic/unsubscribe/', views.TopicUnsubscribeView.as_view(), name='topic-unsubscribe'),
    # path('api/file-serve/<str:file_name>/', ServeProtectedDocumentView.as_view(), name='serve-protected-document'),
    # path('api/file-content/<str:file_name>/', FileContentView.as_view(), name='file-content-view'),
    # path('subscribe/', views.SubscribeView.as_view(), name='subscribe'),
    # path('retrieve-messages/', views.RetrieveMessagesView.as_view(), name='retrieve-messages'),
    # path('publish-message/', views.PublishMessageView.as_view(), name='publish-message'),
    
    path('', include(router.urls)),
    # path('subscribe/<str:broker>/<str:port>/<str:topic>/', views.MQTTSubscribeView.as_view(), name='mqtt-subscribe'),
    # path('publish/<str:broker>/<str:port>/<str:topic>/', views.MQTTPublishView.as_view(), name='mqtt-publish'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)