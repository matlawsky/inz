from rest_framework import permissions

class IsOwner(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to interact with it.
    """
    def has_object_permission(self, request, view, obj):
        # Rhas permission if the owner and request user are the same
        return obj.user == request.user