import 'package:flutter/material.dart';
// import 'package:code_editor/code_editor.dart';
import 'package:frontend/provider/auth_provider.dart';
import 'package:http/http.dart' as http;
// import 'package:http_parser/http_parser.dart';
import 'dart:convert';
// import 'package:mime/mime.dart';
import 'dart:html'; // Required for Blob
import 'package:provider/provider.dart';
import 'dart:core';

class FileSelectionScreen extends StatefulWidget {
  final Function(String) onSelectFile;

  const FileSelectionScreen({Key? key, required this.onSelectFile})
      : super(key: key);

  @override
  _FileSelectionScreenState createState() => _FileSelectionScreenState();
}

class _FileSelectionScreenState extends State<FileSelectionScreen> {
  List<Map<String, dynamic>> _files = [];

  @override
  void initState() {
    super.initState();
    _fetchCodeFiles();
  }

  String getFileNameFromUrl(String url) {
    Uri uri = Uri.parse(url);
    return uri.pathSegments.last;
  }

  Future<void> _fetchCodeFiles() async {
    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    var response = await http.get(
      Uri.parse('http://127.0.0.1:8000/api/code_files/'),
      headers: {'Authorization': 'Bearer ${authProvider.token}'},
    );

    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      print('Received data: $data'); // Debug print to check the data structure
      if (data is List) {
        setState(() {
          _files = List<Map<String, dynamic>>.from(data);
        });
      } else {
        print("Data is not a list");
      }
    } else {
      print("Failed to load code files. Status code: ${response.statusCode}");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Select a File"),
      ),
      body: ListView.builder(
        itemCount: _files.length,
        itemBuilder: (context, index) {
          var file = _files[index];
          return ListTile(
            title: Text(getFileNameFromUrl(file['file']) ??
                'Unknown Name'), // Handle potential nulls
            subtitle:
                Text("Uploaded at: ${file['uploaded_at'] ?? 'Unknown Date'}"),
            onTap: () async {
              // String fileContent = await fetchPythonFileContent('http://127.0.0.1:8000/media/code/your_python_file.py');
              widget.onSelectFile(getFileNameFromUrl(file['file']));
              Navigator.pop(context);
            },
          );
        },
      ),
    );
  }
}
