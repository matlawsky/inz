import 'package:flutter/material.dart';
import 'package:frontend/provider/auth_provider.dart';
import 'package:frontend/screens/data_table.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:provider/provider.dart';

class DeviceDetailScreen extends StatefulWidget {
  final int? deviceId;
  const DeviceDetailScreen({Key? key, this.deviceId}) : super(key: key);

  @override
  _DeviceDetailScreenState createState() => _DeviceDetailScreenState();
}

class _DeviceDetailScreenState extends State<DeviceDetailScreen> {
  Map<String, dynamic>? _deviceDetails;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    if (widget.deviceId != null) {
      _fetchDeviceDetails();
    } else {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Device ID is not available')),
        );
        Navigator.of(context).pop();
      });
    }
  }

  Future<void> _fetchDeviceDetails() async {
    try {
      var authProvider = Provider.of<AuthProvider>(context, listen: false);
      var response = await http.get(
        Uri.parse('http://127.0.0.1:8000/api/devices/${widget.deviceId}/'),
        headers: {'Authorization': 'Bearer ${authProvider.token}'},
      );

      if (response.statusCode == 200) {
        setState(() {
          _deviceDetails = json.decode(response.body);
          _isLoading = false;
        });
      } else if (response.statusCode == 401) {
        bool tokenRefreshed = await authProvider.refreshAccessToken();
        if (tokenRefreshed && mounted) {
          _fetchDeviceDetails();
        } else {
          Navigator.of(context).pushReplacementNamed('/login');
        }
      } else {
        setState(() => _isLoading = false);
      }
    } catch (e) {
      setState(() => _isLoading = false);
    }
  }

  void _navigateToLiveDataScreen(String name, String address) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MQTTDataTableScreen(
          mqttServer: address,
          mqttClientId: name,
          deviceId: widget.deviceId, // Pass the device ID
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_deviceDetails?['name'] ?? "Device Details"),
        actions: [
          IconButton(
            icon: const Icon(Icons.show_chart),
            tooltip: 'Edit MQTT connection settings',
            onPressed: () {
              _navigateToLiveDataScreen(
                  _deviceDetails?['name'], _deviceDetails?['address']);
            },
          ),
        ],
      ),
      body: _isLoading
          ? const Center(child: CircularProgressIndicator())
          : SingleChildScrollView(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildDetailItem('Name', _deviceDetails?['name']),
                  _buildDetailItem(
                      'Description', _deviceDetails?['description']),
                  _buildDetailItem('Address', _deviceDetails?['address']),
                  _buildDetailItem('Port', _deviceDetails?['port']),
                  const SizedBox(height: 20),
                  Text('MQTT Topics',
                      style: Theme.of(context).textTheme.headline6),
                  _buildTopicsList(),
                  SizedBox(height: 20),
                  ElevatedButton(
                    onPressed: () => _navigateToLiveDataScreen(
                        _deviceDetails?['name'], _deviceDetails?['address']),
                    child: const Text('Go to Live Connection!'),
                    style: ElevatedButton.styleFrom(
                        foregroundColor: Theme.of(context).primaryColor),
                  ),
                ],
              ),
            ),
    );
  }

  Widget _buildDetailItem(String label, String? value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Text(
        '$label: ${value ?? 'Not available'}',
        style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildTopicsList() {
    var topics = _deviceDetails?['topics'] as List<dynamic>? ?? [];
    return topics.isEmpty
        ? Text("No MQTT Topics Available",
            style:
                TextStyle(color: Colors.grey[600], fontStyle: FontStyle.italic))
        : ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: topics.length,
            itemBuilder: (context, index) {
              var topic = topics[index];
              return ListTile(
                title: Text(topic['name']),
                onTap: () {
                  print("Tapped on topic: ${topic['name']}");
                },
              );
            },
          );
  }

  @override
  void dispose() {
    super.dispose();
  }
}


// import 'package:flutter/material.dart';
// import 'package:frontend/mqtt_widgets/simple_chart.dart';
// import 'package:frontend/provider/auth_provider.dart';
// import 'package:frontend/screens/data_table.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert';
// import 'package:provider/provider.dart';

// class DeviceDetailScreen extends StatefulWidget {
//   final int? deviceId; // Allow deviceId to be null
//   const DeviceDetailScreen({Key? key, this.deviceId}) : super(key: key);

//   @override
//   _DeviceDetailScreenState createState() => _DeviceDetailScreenState();
// }

// class _DeviceDetailScreenState extends State<DeviceDetailScreen> {
//   Map<String, dynamic>? _deviceDetails;
//   bool _isLoading = true;
//   String _error = '';

//   // Text editing controllers for MQTT configuration
//   final TextEditingController _mqttServerController = TextEditingController();
//   final TextEditingController _mqttClientIdController = TextEditingController();
//   final TextEditingController _mqttTopicController = TextEditingController();

//   @override
//   void initState() {
//     super.initState();
//     if (widget.deviceId != null) {
//       _fetchDeviceDetails();
//     } else {
//       // If deviceId is null, show an error message or navigate back
//       WidgetsBinding.instance.addPostFrameCallback((_) {
//         ScaffoldMessenger.of(context).showSnackBar(
//           const SnackBar(content: Text('Device ID is not available')),
//         );
//         Navigator.of(context).pop(); // Navigate back to the previous screen
//       });
//     }
//   }

//   Future<void> _fetchDeviceDetails() async {
//     try {
//       var authProvider = Provider.of<AuthProvider>(context, listen: false);
//       var response = await http.get(
//         Uri.parse('http://127.0.0.1:8000/api/devices/${widget.deviceId}/'),
//         headers: {'Authorization': 'Bearer ${authProvider.token}'},
//       );

//       if (response.statusCode == 200) {
//         setState(() {
//           _deviceDetails = json.decode(response.body);
//           print(_deviceDetails);
//           _isLoading = false;
//         });
//       } else if (response.statusCode == 401) {
//         bool tokenRefreshed = await authProvider.refreshAccessToken();
//         if (tokenRefreshed && mounted) {
//           _fetchDeviceDetails();
//         } else {
//           Navigator.of(context).pushReplacementNamed('/login');
//         }
//       } else {
//         print('Failed to load devices. Status code: ${response.statusCode}');
//         setState(() {
//           _isLoading = false;
//         });
//       }
//     } catch (e, stacktrace) {
//       print('Exception caught: $e');
//       print('Stacktrace: $stacktrace');
//       setState(() {
//         _isLoading = false;
//       });
//     }
//   }

//   void _navigateToLiveDataScreen(String name, String address) {
//     Navigator.push(
//       context,
//       MaterialPageRoute(
//         builder: (context) => MQTTDataTableScreen(
//           mqttServer: address,
//           mqttClientId: name,
//         ),
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(_deviceDetails?['name'] ?? "Device Details"),
//         actions: [
//           IconButton(
//             icon: const Icon(Icons.show_chart),
//             onPressed: () {
//               _navigateToLiveDataScreen(
//                   _deviceDetails?['name'], _deviceDetails?['address']);
//             },
//             tooltip: 'Open Live MQTT connection',
//           ),
//         ],
//       ),
//       body: Row(
//         children: [
//           Expanded(
//             child: SingleChildScrollView(
//               padding: const EdgeInsets.all(16.0),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   _buildDetailItem('Name', _deviceDetails?['name']),
//                   _buildDetailItem(
//                       'Description', _deviceDetails?['description']),
//                   _buildDetailItem('Address', _deviceDetails?['address']),
//                   _buildDetailItem('Port', _deviceDetails?['port']),
//                   const SizedBox(height: 20),
//                   _buildTopicsList(),
//                   ElevatedButton(
//                     onPressed: () {
//                       _navigateToLiveDataScreen(
//                           _deviceDetails?['name'], _deviceDetails?['address']);
//                     },
//                     child: const Text('Go to live connection!'),
//                   )
//                 ],
//               ),
//             ),
//           )
//         ],
//       ),
//     );
//   }

//   Widget _buildDetailItem(String label, String? value) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(vertical: 10.0),
//       child: Text(
//         '$label: ${value ?? 'Not available'}',
//         style: const TextStyle(fontSize: 18),
//       ),
//     );
//   }

//   Widget _buildTopicsList() {
//     var topics = _deviceDetails?['topics'] as List<dynamic>? ?? [];
//     return ListView.builder(
//       shrinkWrap: true,
//       physics: const NeverScrollableScrollPhysics(),
//       itemCount: topics.length,
//       itemBuilder: (context, index) {
//         var topic = topics[index];
//         return ListTile(
//           title: Text(topic['name']),
//           onTap: () {
//             // Perform action on tap
//             print("Tapped on topic: ${topic['name']}");
//           },
//         );
//       },
//     );
//   }

//   @override
//   void dispose() {
//     _mqttServerController.dispose();
//     _mqttClientIdController.dispose();
//     _mqttTopicController.dispose();
//     super.dispose();
//   }
// }



// import 'package:flutter/material.dart';
// import 'package:frontend/mqtt_widgets/simple_chart.dart';
// import 'package:frontend/provider/auth_provider.dart';
// import 'package:frontend/screens/data_table.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert';
// import 'package:provider/provider.dart';

// class DeviceDetailScreen extends StatefulWidget {
//   final int? deviceId; // Allow deviceId to be null
//   const DeviceDetailScreen({Key? key, this.deviceId}) : super(key: key);

//   @override
//   _DeviceDetailScreenState createState() => _DeviceDetailScreenState();
// }

// class _DeviceDetailScreenState extends State<DeviceDetailScreen> {
//   Map<String, dynamic>? _deviceDetails;
//   // Map<String, dynamic>? _activeTopics;
//   bool _isLoading = true;
//   String _error = '';

//   // Text editing controllers for MQTT configuration
//   final TextEditingController _mqttServerController = TextEditingController();
//   final TextEditingController _mqttClientIdController = TextEditingController();
//   final TextEditingController _mqttTopicController = TextEditingController();

//   @override
//   void initState() {
//     super.initState();
//     if (widget.deviceId != null) {
//       _fetchDeviceDetails();
//     } else {
//       // If deviceId is null, show an error message or navigate back
//       WidgetsBinding.instance.addPostFrameCallback((_) {
//         ScaffoldMessenger.of(context).showSnackBar(
//           const SnackBar(content: Text('Device ID is not available')),
//         );
//         Navigator.of(context).pop(); // Navigate back to the previous screen
//       });
//     }
//   }

//   Future<void> _fetchDeviceDetails() async {
//     try {
//       var authProvider = Provider.of<AuthProvider>(context, listen: false);
//       var response = await http.get(
//         Uri.parse('http://127.0.0.1:8000/api/devices/${widget.deviceId}/'),
//         headers: {'Authorization': 'Bearer ${authProvider.token}'},
//       );

//       if (response.statusCode == 200) {
//         setState(() {
//           _deviceDetails = json.decode(response.body);
//           print(_deviceDetails);
//           _isLoading = false;
//         });
//       } else if (response.statusCode == 401) {
//         // If unauthorized, attempt token refresh
//         bool tokenRefreshed = await authProvider.refreshAccessToken();

//         if (tokenRefreshed && mounted) {
//           // If token is refreshed and the widget is still in the tree, retry fetching devices
//           _fetchDeviceDetails();
//         } else {
//           // Handle failed token
//           Navigator.of(context).pushReplacementNamed(
//               '/login'); // assuming '/login' is the route for your login screen
//         }
//       } else {
//         print(
//             'Failed to load devices. Status code: ${response.statusCode}'); // Debugging information
//         setState(() {
//           _isLoading = false;
//         });
//       }
//     } catch (e, stacktrace) {
//       print('Exception caught: $e');
//       print('Stacktrace: $stacktrace');
//       if (mounted) {
//         setState(() {
//           _isLoading = false;
//         });
//       }
//     }
//   }

//   Future<void> _fetchTopic(int? topic) async {
//     try {
//       var authProvider = Provider.of<AuthProvider>(context, listen: false);
//       var response = await http.get(
//         Uri.parse('http://127.0.0.1:8000/api/topics/${topic}/'),
//         headers: {'Authorization': 'Bearer ${authProvider.token}'},
//       );

//       if (response.statusCode == 200) {
//         setState(() {
//           _deviceDetails = json.decode(response.body);
//           print(_deviceDetails);
//           _isLoading = false;
//         });
//       } else if (response.statusCode == 401) {
//         // If unauthorized, attempt token refresh
//         bool tokenRefreshed = await authProvider.refreshAccessToken();

//         if (tokenRefreshed && mounted) {
//           // If token is refreshed and the widget is still in the tree, retry fetching devices
//           _fetchTopic(topic);
//         } else {
//           // Handle failed token
//           Navigator.of(context).pushReplacementNamed(
//               '/login'); // assuming '/login' is the route for your login screen
//         }
//       } else {
//         print(
//             'Failed to load devices. Status code: ${response.statusCode}'); // Debugging information
//         setState(() {
//           _isLoading = false;
//         });
//       }
//     } catch (e, stacktrace) {
//       print('Exception caught: $e');
//       print('Stacktrace: $stacktrace');
//       if (mounted) {
//         setState(() {
//           _isLoading = false;
//         });
//       }
//     }
//   }

//   Future<void> _delTopic(int? topic) async {
//     try {
//       var authProvider = Provider.of<AuthProvider>(context, listen: false);
//       var response = await http.delete(
//         Uri.parse('http://127.0.0.1:8000/api/topics/${topic}/'),
//         headers: {'Authorization': 'Bearer ${authProvider.token}'},
//       );

//       if (response.statusCode == 200) {
//         setState(() {
//           _deviceDetails = json.decode(response.body);
//           print(_deviceDetails);
//           _isLoading = false;
//         });
//       } else if (response.statusCode == 401) {
//         // If unauthorized, attempt token refresh
//         bool tokenRefreshed = await authProvider.refreshAccessToken();

//         if (tokenRefreshed && mounted) {
//           // If token is refreshed and the widget is still in the tree, retry fetching devices
//           _fetchTopic(topic);
//         } else {
//           // Handle failed token
//           Navigator.of(context).pushReplacementNamed(
//               '/login'); // assuming '/login' is the route for your login screen
//         }
//       } else {
//         print(
//             'Failed to load devices. Status code: ${response.statusCode}'); // Debugging information
//         setState(() {
//           _isLoading = false;
//         });
//       }
//     } catch (e, stacktrace) {
//       print('Exception caught: $e');
//       print('Stacktrace: $stacktrace');
//       if (mounted) {
//         setState(() {
//           _isLoading = false;
//         });
//       }
//     }
//   }

//   void _navigateToLiveDataScreen(String name, String address) {
//     Navigator.push(
//       context,
//       MaterialPageRoute(
//         builder: (context) => MQTTDataTableScreen(
//           mqttServer: address,
//           mqttClientId: name,
//         ),
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(_deviceDetails?['name'] ?? "Device Details"),
//         actions: [
//           IconButton(
//             icon: const Icon(Icons.show_chart),
//             onPressed: () {
//               _navigateToLiveDataScreen(
//                   _deviceDetails?['name'], _deviceDetails?['address']);
//             },
//             tooltip: 'Open Live MQTT connection',
//           ),
//         ],
//       ),
//       body: Row(
//         children: [
//           Expanded(
//             child: SingleChildScrollView(
//               padding: const EdgeInsets.all(16.0),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   _buildDetailItem('Name', _deviceDetails?['name']),
//                   _buildDetailItem(
//                       'Description', _deviceDetails?['description']),
//                   _buildDetailItem('Address', _deviceDetails?['address']),
//                   _buildDetailItem('Port', _deviceDetails?['port']),
//                   const SizedBox(height: 20),
//                   ElevatedButton(
//                     onPressed: () {
//                       _navigateToLiveDataScreen(
//                           _deviceDetails?['name'], _deviceDetails?['address']);
//                     },
//                     child: const Text('Go to live connection!'),
//                   )
//                 ],
//               ),
//             ),
//           )
//         ],
//       ),
//     );
//   }

//   Widget _buildDetailItem(String label, String? value) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(vertical: 10.0),
//       child: Text(
//         '$label: ${value ?? 'Not available'}',
//         style: const TextStyle(fontSize: 18),
//       ),
//     );
//   }

//   @override
//   void dispose() {
//     _mqttServerController.dispose();
//     _mqttClientIdController.dispose();
//     _mqttTopicController.dispose();
//     super.dispose();
//   }
// }
