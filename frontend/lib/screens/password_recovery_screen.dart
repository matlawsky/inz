import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class PasswordRecoveryScreen extends StatefulWidget {
  @override
  PasswordRecoveryScreenState createState() => PasswordRecoveryScreenState();
}

class PasswordRecoveryScreenState extends State<PasswordRecoveryScreen> {
  final _formKey = GlobalKey<FormState>();
  String _username = '';
  String _selectedQuestion = '';
  String _answer = '';
  bool _isLoading = false;
  bool _isUsernameSubmitted = false;
  bool _loadingQuestions = true;
  bool _passwordChangePrompt = false;

  @override
  void initState() {
    super.initState();
  }

  Future<void> _fetchSecurityQuestions() async {
    try {
      var response = await http.get(
        Uri.parse(
            'http://127.0.0.1:8000/api/security-question/?username=$_username'),
      );

      if (response.statusCode == 200) {
        var data = json.decode(response.body);
        setState(() {
          _selectedQuestion = data['question'];
          _loadingQuestions = false;
          _isUsernameSubmitted = true;
        });
      } else {
        // Handle error response
      }
    } catch (e) {
      // Handle exceptions
    }
  }

  Future<void> _submitAnswer() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      setState(() {
        _isLoading = true;
      });

      var response = await http.get(
        Uri.parse(
            'http://127.0.0.1:8000/api/verify-answer/?username=$_username&question=$_selectedQuestion&answer=$_answer'),
      );

      if (response.statusCode == 200) {
        // Handle successful response
        setState(() {
          _passwordChangePrompt = true;
        });
      } else {
        // Handle error response
      }

      setState(() {
        _isLoading = false;
      });
    }
  }

  Widget _buildUsernameForm() {
    return TextFormField(
      decoration: const InputDecoration(labelText: 'Username'),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please enter your username';
        }
        return null;
      },
      onSaved: (value) {
        _username = value!;
      },
    );
  }

  Widget _buildSecurityQuestionForm() {
    return Column(
      children: <Widget>[
        Text(_selectedQuestion),
        TextFormField(
          decoration: const InputDecoration(labelText: 'Your Answer'),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Please enter your answer';
            }
            return null;
          },
          onSaved: (value) {
            _answer = value!;
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Password Recovery'),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _isUsernameSubmitted
                  ? _loadingQuestions
                      ? const CircularProgressIndicator()
                      : _buildSecurityQuestionForm()
                  : _buildUsernameForm(),
              const SizedBox(height: 20),
              _isLoading
                  ? const CircularProgressIndicator()
                  : ElevatedButton(
                      onPressed: _isUsernameSubmitted
                          ? _submitAnswer
                          : () async {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();
                                await _fetchSecurityQuestions();
                              }
                            },
                      child: Text(_isUsernameSubmitted
                          ? 'Submit Answer'
                          : 'Submit Username'),
                    ),
              if (_passwordChangePrompt)
                const Text("Password change prompt here"),
            ],
          ),
        ),
      ),
    );
  }
}
