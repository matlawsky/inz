import 'package:flutter/material.dart';
import 'package:frontend/screens/password_recovery_screen.dart';
import 'package:frontend/screens/register_screen.dart';
import 'package:provider/provider.dart';
import '../provider/auth_provider.dart'; // Make sure you have this file created with the AuthProvider class

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  String _username = '';
  String _password = '';

  Future<void> _login() async {
    print("Login function is called");
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      print("Login function is validated");
      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      print("Login function is past provider");
      bool loggedIn = await authProvider.login(_username, _password);
      print("Logged in: $loggedIn");
      if (loggedIn) {
        // Navigate to home screen or another appropriate screen
        Navigator.of(context).pushReplacementNamed('/home');
      } else {
        // Show error message
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Failed to log in, please try again.')),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final isLoading = Provider.of<AuthProvider>(context).isLoading;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                decoration: const InputDecoration(labelText: 'Username'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your username';
                  }
                  return null;
                },
                onSaved: (value) => _username = value ?? '',
              ),
              TextFormField(
                decoration: const InputDecoration(labelText: 'Password'),
                obscureText: true,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your password';
                  }
                  return null;
                },
                onSaved: (value) => _password = value ?? '',
              ),
              const SizedBox(height: 20),
              isLoading
                  ? const CircularProgressIndicator()
                  : ElevatedButton(
                      onPressed: _login,
                      child: const Text('Login'),
                    ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24.0,
                  vertical: 10,
                ),
                child: TextButton(
                  onPressed: () {
                    // Push the Password Recovery Screen
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => PasswordRecoveryScreen()));
                  },
                  child: const Text('Forgot Password?'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24.0,
                  vertical: 10,
                ),
                child: TextButton(
                  onPressed: () {
                    // Push the Password Recovery Screen
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => RegisterScreen()));
                  },
                  child: const Text('Create new account.'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
