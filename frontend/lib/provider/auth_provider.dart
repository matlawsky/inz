import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AuthProvider with ChangeNotifier {
  bool _isLoading = false;
  String _token = '';
  String _refreshToken = '';
  bool get isLoading => _isLoading;
  bool get isAuth => _token.isNotEmpty;

  String get token => _token;
  String get refreshToken => _refreshToken;

  void setLoading(bool loading) {
    _isLoading = loading;
    notifyListeners();
  }

  Future<bool> login(String username, String password) async {
    setLoading(true);
    try {
      var response = await http.post(
        Uri.parse('http://127.0.0.1:8000/api/token/'),
        headers: {"Content-Type": "application/json"},
        body: json.encode({'username': username, 'password': password}),
      );
      if (response.statusCode == 200) {
        var responseData = json.decode(response.body);
        _token = responseData['access'];
        _refreshToken = responseData['refresh'];
        notifyListeners();
        setLoading(false);
        return true;
      } else {
        setLoading(false);
        return false;
      }
    } catch (e) {
      setLoading(false);
      return false;
    }
  }

  Future<bool> refreshAccessToken() async {
    try {
      var response = await http.post(
        Uri.parse('http://127.0.0.1:8000/api/token/refresh/'),
        headers: {"Content-Type": "application/json"},
        body: json.encode({'refresh': _refreshToken}),
      );
      if (response.statusCode == 200) {
        var responseData = json.decode(response.body);
        _token = responseData['access'];
        notifyListeners();
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  Future<bool> recoverPassword(
      String username, String question, String answer) async {
    setLoading(true);
    try {
      var response = await http.post(
        Uri.parse('http://127.0.0.1:8000/api/recover-password/'),
        headers: {"Content-Type": "application/json"},
        body: json.encode(
            {'username': username, 'question': question, 'answer': answer}),
      );
      setLoading(false);
      return response.statusCode == 200;
    } catch (e) {
      setLoading(false);
      return false;
    }
  }

  Future<bool> changePassword(
      String username, String oldPassword, String newPassword) async {
    setLoading(true);
    try {
      var response = await http.post(
        Uri.parse('http://127.0.0.1:8000/api/change-password/'),
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          'username': username,
          'old_password': oldPassword,
          'new_password': newPassword,
        }),
      );
      setLoading(false);
      return response.statusCode == 200;
    } catch (e) {
      setLoading(false);
      return false;
    }
  }

  Future<bool> register(String username, String password) async {
    setLoading(true);
    try {
      var response = await http.post(
        Uri.parse('http://127.0.0.1:8000/api/register/'),
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          'username': username,
          'password': password,
        }),
      );
      setLoading(false);
      return ((response.statusCode == 200) || (response.statusCode == 201));
    } catch (e) {
      setLoading(false);
      return false;
    }
  }
}
